<?php
$server = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($server, $username, $password, $database);
if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Kiểm tra xem có tham số "id" được truyền vào hay không
if (isset($_GET['id'])) {
    $studentId = $_GET['id'];

    // Xóa sinh viên từ cơ sở dữ liệu
    $sql = "DELETE FROM students WHERE id = $studentId";
    if ($conn->query($sql) === TRUE) {
        // Gửi phản hồi JSON thành công
        echo json_encode(array('success' => true));
        exit();
    } else {
        echo json_encode(array('success' => false, 'message' => 'Lỗi khi xóa sinh viên: ' . $conn->error));
        exit();
    }
}

$conn->close();
?>