<?php
$server = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($server, $username, $password, $database);
if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}
$full_name = '';
$gender = '';
$department = '';
$birthdate = '';
$address = '';
if (isset($_GET['id'])) {
    $studentId = $_GET['id'];

    $sql = "SELECT * FROM students WHERE id = $studentId";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $student = $result->fetch_assoc();
        $full_name = $student['full_name'];
        $gender = $student['gender'];
        $department = $student['department'];
        $birthdate = $student['birthdate'];
        $address = $student['address'];
    } else {
        die("Không tìm thấy sinh viên có ID = $studentId");
    }
} else {
    die("Thiếu tham số ID");
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['full_name'])) {
        $full_name = $_POST['full_name'];
    }

    if (isset($_POST['gender'])) {
        $gender = $_POST['gender'];
    }

    if (isset($_POST['department'])) {
        $department = $_POST['department'];
    }

    if (isset($_POST['birthdate'])) {
        $birthdate = $_POST['birthdate'];
    }

    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }

    $departmentMappings = array(
        'MAT' => 'Khoa học máy tính',
        'KDL' => 'Khoa học vật liệu'
    );

    if (isset($departmentMappings[$department])) {
        $department = $departmentMappings[$department];
    }

    $updateSql = "UPDATE students SET full_name = '$full_name', gender = '$gender', department = '$department', birthdate = '$birthdate', address = '$address' WHERE id = $studentId";

    if ($conn->query($updateSql) === TRUE) {
        header('Location: students.php');
        exit();
    } else {
        die("Lỗi khi cập nhật thông tin sinh viên: " . $conn->error);
    }
}

$conn->close();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #07f;
            display: inline-block;
            margin: 10% 20%;
        }

        .label {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: limegreen;
            text-align: center;
            width: 90px;
            padding: 10px 0px 5px;
            margin: 5px 10px 5px 40px;

        }

        .submit {
            border: 2px solid #07f;
            background-color: limegreen;
            color: #fff;
            text-align: center;
            width: 130px;
            border-radius: 10px;
            padding: 15px 0px 10px;
            margin: 10px 180px 20px;
        }

        .input_name {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 300px;
            padding: 10px 10px 5px;
            margin: 0px 50px 0px 0px;
        }

        #department {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 140px;
            padding: 10px 0px 5px;
        }

        .radio_gender {
            margin: 10px;
        }

        .input_birthdate {
            display: inline-block;
            border: 2px solid #07f;
            width: 136px;
            outline: none;
            padding: 10px 0px 5px;
        }

        .address {
            display: inline-block;
            border: 2px solid #07f;
            width: 320px;
            outline: none;
            padding: 10px 0px 10px;
        }
    </style>
</head>

<body>
    <form method="POST" class="register">
        <label for="full_name" class="label">Họ và tên:</label>
        <input type="text" id="full_name" name="full_name" class="input_name" value="<?php echo $student['full_name']; ?>" required><br>

        <label for="gender" class="label">Giới tính</label>
        <?php
        $genders = array(
            'Nam' => 'Nam',
            'Nữ' => 'Nữ'
        );

        foreach ($genders as $genderValue => $genderText) {
            echo '<label>';
            echo '<input type="radio" name="gender" class="radio_gender" value="' . $genderValue . '"';
            if ($genderValue === $student['gender']) {
                echo 'checked';
            }
            echo '>' . $genderText;
            echo '</label>';
        }
        ?><br>

        <label for="department" class="label">Phân khoa<span class="star"> *</span></label>
        <select name="department" id="department" required>
            <option value="">--Chọn phân khoa--</option>
            <?php
            $departments = array(
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu'
            );

            foreach ($departments as $departmentValue => $departmentText) {
                echo '<option value="' . $departmentValue . '"';
                if ($departmentValue === $student['department']) {
                    echo 'selected';
                }
                echo '>' . $departmentText . '</option>';
            }
            ?>
        </select><br>
        <label for="birthdate" class="label">Ngày sinh</label>
        <input type="date" class="input_birthdate" name="birthdate" value="<?php echo $student['birthdate']; ?>" required><br>
        <label for="address" class="label">Địa chỉ:</label>
        <input type="text" class="address" name="address" value="<?php echo $student['address']; ?>"><br>
        <button type="submit" class="submit">Xác nhận</button>
    </form>
</body>

</html>