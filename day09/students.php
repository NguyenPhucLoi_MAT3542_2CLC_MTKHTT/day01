<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Student List</title>
    <style>
        .student-list {
            display: inline-block;
            margin: 50px 50px;
        }

        .label {
            display: inline-block;
            margin: 5px 34px 5px 100px;

        }

        .label_keyword {
            margin: 5px 15px 5px 100px;

        }

        .search {
            border: 2px solid blue;
            background-color: #06f;
            color: #fff;
            text-align: center;
            width: 120px;
            border-radius: 10px;
            padding: 15px 0px 10px;
            margin: 10px 0px 10px 220px;
        }

        #department {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 180px;
            padding: 10px 0px 5px;
            background-color: lightblue;
            margin: 0px 20px 20px;
        }

        #keyword {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 178px;
            padding: 10px 0px 5px;
            background-color: lightblue;
            margin: 0px 0px 10px 20px;
        }

        .button {
            width: 50px;
            border: 2px solid blue;
            background-color: #09f;
            color: #fff;
            padding: 5px;

        }

        th {
            padding: 0px 10px 5px 10px;
            text-align: left;
        }

        td {
            padding: 3px 10px 5px 10px;
            text-align: left;
        }

        th.action,
        td.action {
            padding-left: 100px;
        }

        .add-students {
            display: flex;
            align-items: center;
        }

        .add {
            border: 2px solid blue;
            background-color: #06f;
            color: #fff;
            text-align: center;
            width: 90px;
            border-radius: 10px;
            padding: 10px 0px 10px;
            margin: 10px 0px 0px 258px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {

            function resetSearch() {
                $("#department").val("");
                $("#keyword").val("");
                getStudentList();
            }

            $(".search").click(function() {
                resetSearch();
            });

            function getStudentList() {
                var department = $("#department").val();
                var keyword = $("#keyword").val();

                $.ajax({
                    url: "search.php",
                    type: "GET",
                    data: {
                        department: department,
                        keyword: keyword
                    },
                    success: function(response) {
                        $("#student-list-body").html(response);
                        $("#numStudents").text($("#student-list-body tr").length);
                    },
                    error: function(xhr, status, error) {
                        alert("Error: " + error);
                    }
                });
            }

            $("#department").change(function() {
                getStudentList();
            });

            $("#keyword").on("keyup", function() {
                getStudentList();
            });

            // Lấy danh sách sinh viên ban đầu
            getStudentList();
        });

        function deleteStudent(id) {
            if (confirm("Bạn muốn xóa sinh viên này?")) {
                $.ajax({
                    url: 'delete_students.php',
                    type: 'GET',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    success: function(response) {
                        if (response.success) {
                            $("#student_" + id).remove();
                            location.reload();
                        } else {
                            alert('Lỗi khi xóa sinh viên: ' + response.message);
                        }
                    },
                    error: function(xhr, status, error) {
                        alert('Lỗi khi gửi yêu cầu AJAX: ' + error);
                    }
                });
            }
        }


        function editStudent(id) {
            window.location.href = 'update_students.php?id=' + id;
        }
    </script>
</head>

<body>
    <div class="student-list">
        <form method="get">
            <label for="department" class="label">Khoa</span></label>
            <select name="department" id="department">
                <option value=""></option>
                <?php
                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu',
                );

                foreach ($departments as $key => $value) {
                    $selected = ($_GET['department'] == $key) ? 'selected' : '';
                    echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
                }

                ?>
            </select><br>

            <label for="keyword" class="label_keyword">Từ khóa</label>
            <input type="text" name="keyword" id="keyword"><br>

            <button type="button" class="search" onclick="resetSearch()">Reset</button>
        </form>
        <div class="add-students">
            <p>Số sinh viên tìm thấy: <span id="numStudents"></span></p>
            <form action="register.php">
                <button type="submit" class="add">Thêm</button>
            </form>
        </div>
        <table>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th class="action">Action</th>
                </tr>

            </thead>
            <tbody id="student-list-body">

            </tbody>
        </table>
    </div>
</body>

</html>