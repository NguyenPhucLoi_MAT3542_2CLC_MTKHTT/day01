<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #07f;
            display: inline-block;
            margin: 10% 20%;
        }

        .label {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: limegreen;
            text-align: center;
            width: 150px;
            padding: 10px 0px 5px;
            margin: 5px 10px 5px 40px;

        }

        .submit {
            border: 2px solid #07f;
            background-color: limegreen;
            color: #fff;
            text-align: center;
            width: 130px;
            border-radius: 10px;
            padding: 15px 0px 10px;
            margin: 10px 180px 20px;
        }

        .input_name {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 300px;
            padding: 10px 10px 5px;
            margin: 0px 50px 0px 0px;
        }

        #birth_date {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 65px;
            padding: 10px 0px 5px;
        }

        #address {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 130px;
            padding: 10px 0px 5px;
        }

        .radio_gender {
            margin: 10px;
        }

        #info {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            padding: 10px 0px 10px;
        }

        .errorMessage {
            color: red;
            margin: 10px;
        }

        .list {
            list-style-type: none;
        }

        .label_info {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: limegreen;
            text-align: center;
            width: 150px;
            padding: 10px 0px 5px;
            margin: 5px 10px 5px 40px;
        }
    </style>
</head>

<body>
    <div class="register">
        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $fullname = $_POST['name'];
            $gender = $_POST['gender'];
            $year = $_POST['year'];
            $month = $_POST['month'];
            $day = $_POST['day'];
            $city = $_POST['city'];
            $district = $_POST['district'];
            $info  = $_POST['info'];
            $genders = array(
                1 => 'Nam',
                2 => 'Nữ'
            );
            $gender_label = $genders[$gender];

            $citys = array(
                'HN' => 'Hà Nội',
                'HCM' => 'TP. Hồ Chí Minh'
            );
            $city_label = $citys[$city];

            $districts = array(
                'HM' => 'Hoàng Mai',
                'TT' => 'Thanh Trì',
                'NTL' => 'Nam Từ Liêm',
                'HD' => 'Hà Đông',
                'CG' => 'Cầu Giấy',
                'Q1' => 'Quận 1',
                'Q2' => 'Quận 2',
                'Q3' => 'Quận 3',
                'Q7' => 'Quận 7',
                'Q9' => 'Quận 9'
            );
            $district_label = $districts[$district];

            echo "<p><span class='label'>Họ và tên</span> $fullname</p>";
            echo "<p><span class='label'>Giới tính</span> $gender_label</p>";
            echo "<p><span class='label'>Ngày sinh</span> $day/$month/$year</p>";
            echo "<p><span class='label'>Địa chỉ</span> $city_label - $district_label </p>";
            echo "<p><span class='label'>Thông tin khác</span> $info </p>";
        }
        ?>
    </div>
</body>

</html>