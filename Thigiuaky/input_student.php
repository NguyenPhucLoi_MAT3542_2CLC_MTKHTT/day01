<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #07f;
            display: inline-block;
            margin: 10% 20%;
        }

        .label {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: limegreen;
            text-align: center;
            width: 150px;
            padding: 10px 0px 5px;
            margin: 5px 10px 5px 40px;

        }

        .submit {
            border: 2px solid #07f;
            background-color: limegreen;
            color: #fff;
            text-align: center;
            width: 130px;
            border-radius: 10px;
            padding: 15px 0px 10px;
            margin: 10px 180px 20px;
        }

        .input_name {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 300px;
            padding: 10px 10px 5px;
            margin: 0px 50px 0px 0px;
        }

        #birth_date {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 65px;
            padding: 10px 0px 5px;
        }

        #address {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 130px;
            padding: 10px 0px 5px;
        }

        .radio_gender {
            margin: 10px;
        }

        #info {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            padding: 10px 0px 10px;
        }

        .errorMessage {
            color: red;
            margin: 10px;
        }

        .list {
            list-style-type: none;
        }

        .label_info {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: limegreen;
            text-align: center;
            width: 150px;
            padding: 10px 0px 5px;
            margin: 5px 10px 5px 40px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#registrationForm").submit(function(event) {
                event.preventDefault();

                var errors = [];

                var name = $(".input_name").val();
                if (name.trim() === '') {
                    errors.push("Hãy nhập tên.");
                }

                var birth_date = $("#birth_date").val();
                if (birth_date === '') {
                    errors.push("Hãy chọn ngày sinh.");
                }
                var address = $("#address").val();
                if (address === '') {
                    errors.push("Hãy chọn địa chỉ.");
                }
                var gender = $(".radio_gender:checked").val();
                if (gender === undefined) {
                    errors.push("Hãy chọn giới tính.");
                }

                var message = "";
                if (errors.length > 0) {
                    message = '<ul class= "list">';
                    for (var i = 0; i < errors.length; i++) {
                        message += '<li>' + errors[i] + '</li>';
                    }
                    message += '</ul>';
                }
                $(".errorMessage").html(message);

                if (errors.length === 0) {
                    $(this).unbind('submit').submit();
                }
            });
        });
    </script>
</head>

<body>
    <div class="register">
        <div class="errorMessage"></div>
        <form id="registrationForm" method="POST" action="regist_student.php">
            <label for="name" class="label">Họ và tên</label>
            <input type="textbox" class="input_name" name="name"><br>
            <label for="gender" class="label">Giới tính</label>
            <?php
            $genders = array(
                1 => 'Nam',
                2 => 'Nữ'
            );

            $count = count($genders);
            $keys = array_keys($genders);

            for ($i = 0; $i < $count; $i++) {
                $genderValue = $keys[$i];
                $genderText = $genders[$genderValue];
                echo '<label>';
                echo '<input type="radio" name="gender" class="radio_gender"  value="' . $genderValue . '">' . $genderText;
                echo '</label>';
            }
            ?><br>
            <label for="" class="label">Ngày sinh</label>
            <label for="year">Năm</label>
            <select name="year" id="birth_date">
                <option value="" selected></option>
                <?php
                $currentYear = date('Y');
                for ($year = $currentYear - 40; $year <= $currentYear - 15; $year++) {
                    echo '<option value="' . $year . '">' . $year . '</option>';
                }
                ?>
            </select>

            <label for="month">Tháng</label>
            <select name="month" id="birth_date">
                <option value="" selected></option>
                <?php
                for ($month = 1; $month <= 12; $month++) {
                    echo '<option value="' . $month . '">' . $month . '</option>';
                }
                ?>
            </select>

            <label for="day">Ngày</label>
            <select name="day" id="birth_date">
                <option value="" selected></option>
                <?php
                for ($day = 1; $day <= 31; $day++) {
                    echo '<option value="' . $day . '">' . $day . '</option>';
                }
                ?>
            </select><br>

            <label for="" class="label">Địa chỉ</label>
            <label for="city" class="">Thành phố</label>
            <select name="city" id="address">
                <option value="" selected></option>
                <?php
                $city = array(
                    'HN' => 'Hà Nội',
                    'HCM' => 'TP. Hồ Chí Minh'
                );

                foreach ($city as $key => $value) {
                    echo '<option value="' . $key . '">' . $value . '</option>';
                }
                ?>
            </select>
            <label for="" class="">Quận</label>
            <select name="district" id="address">
                <option value="" selected></option>
                <?php
                $district = array(
                    'HM' => 'Hoàng Mai',
                    'TT' => 'Thanh Trì',
                    'NTL' => 'Nam Từ Liêm',
                    'HD' => 'Hà Đông',
                    'CG' => 'Cầu Giấy',
                    'Q1' => 'Quận 1',
                    'Q2' => 'Quận 2',
                    'Q3' => 'Quận 3',
                    'Q7' => 'Quận 7',
                    'Q9' => 'Quận 9'
                );

                foreach ($district as $key => $value) {
                    echo '<option value="' . $key . '">' . $value . '</option>';
                }
                ?>
            </select><br>
            <label for="info" class="label_info">Thông tin khác</label>
            <textarea name="info" id="info" rows="4" cols="50"></textarea>
            <button type="submit" class="submit">Đăng ký</button>
        </form>
    </div>
</body>

</html>