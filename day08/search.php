<?php
$server = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

$conn = new mysqli($server, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

$department = isset($_GET['department']) ? $_GET['department'] : '';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';

$sql = "SELECT * FROM students";
if (!empty($department)) {
    if ($department == 'KDL') {
        $sql .= " WHERE department = 'Khoa học vật liệu'";
    } elseif ($department == 'MAT') {
        $sql .= " WHERE department = 'Khoa học máy tính'";
    }
    if (!empty($keyword)) {
        $sql .= " AND full_name LIKE '%$keyword%'";
    }
} else {
    if (!empty($keyword)) {
        $sql .= " WHERE full_name LIKE '%$keyword%'";
    }
}

$result = $conn->query($sql);
$count = 1;
$result = mysqli_query($conn, $sql);
$count = 1;
$num_rows = mysqli_num_rows($result);
if ($result) {
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $count . "</td>";
        echo "<td>" . $row['full_name'] . "</td>";
        echo "<td>" . $row['department'] . "</td>";
        echo '<td class="action"><button onclick="deleteStudent(' . $row['id'] . ')" class="button">Xóa</button> <button onclick="editStudent(' . $row['id'] . ')" class="button">Sửa</button></td>';
        echo "</tr>";
        $count++;
    }
} else {
    echo "Lỗi truy vấn: " . $conn->error; // In ra thông báo lỗi nếu truy vấn thất bại
}

$conn->close();
