<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <style>
        .username-container {
            border: 2px solid #07f;
            display: inline-block;
            margin: 10% 30%;
        }

        .timezone {
            border: 1px;
            padding: 10px;
            margin: 20px 80px 10px;
            display: inline-block;
            background-color: #ccc;
            width: 340px;
        }

        .label {
            display: inline-block;
            padding: 10px;
            border: 2px solid #07f;
            background-color: #09f;
            width: 150px;
            margin: 5px 10px 10px 80px;
            color: #fff;
        }

        .input {
            border: 2px solid;
            padding: 10px;
            color: #07f;
            font-weight: bold;
            outline: none;
            background-color: #fff;
            width: 150px;
        }

        .login {
            display: inline-block;
            padding: 15px 0px;
            border: 2px solid #04f;
            background-color: #09f;
            margin: 25px 0px 15px 185px;
            color: #fff;
            border-radius: 10px;
            width: 150px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="username-container">
        <div class="timezone">
            <?php
            // Đặt timezone là Việt Nam
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            // Hiển thị ngày, giờ hiện tại theo timezone Việt Nam
            echo "Bây giờ là: " . date("H:i") . ", thứ " . (date("N") + 1) . " ngày " . date("d/m/Y");
            ?>
        </div>
        <form>
            <label class="label">Tên đăng nhập</label>
            <input class="input"><br>
            <label class="label">Mật khẩu</label>
            <input class="input"><br>
            <button class="login">Đăng nhập</button>
        </form>
    </div>
</body>

</html>