<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #07f;
            display: inline-block;
            margin: 10% 20%;
        }

        .label {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: limegreen;
            text-align: center;
            width: 90px;
            padding: 10px 0px 5px;
            margin: 5px 30px 5px 40px;

        }

        .submit {
            border: 2px solid #07f;
            background-color: limegreen;
            color: #fff;
            text-align: center;
            width: 130px;
            border-radius: 10px;
            padding: 15px 0px 10px;
            margin: 30px 200px 20px;
        }

        .img {
            max-width: 150px;
            max-height: 150px;
            display: inline-block;
            vertical-align: top;
            margin-top: 5px;
        }
    </style>
</head>

<body>
    <div class="register">
        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $fullname = $_POST['name'];
            $gender = $_POST['gender'];
            $department = $_POST['department'];
            $birthdate = $_POST['birthdate'];
            $address = $_POST['address'];

            $genders = array(
                0 => 'Nam',
                1 => 'Nữ'
            );
            $gender_label = $genders[$gender];
            $departments = array(
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu'
            );
            $department_label = $departments[$department];

            echo "<p><span class='label'>Họ và tên</span> $fullname</p>";
            echo "<p><span class='label'>Giới tính</span> $gender_label</p>";
            echo "<p><span class='label'>Phân khoa</span> $department_label</p>";
            echo "<p><span class='label'>Ngày sinh</span> $birthdate</p>";
            echo "<p><span class='label'>Địa chỉ</span> $address</p>";
            if (isset($_FILES['imageUpload'])) {
                $file = $_FILES['imageUpload'];
                $file_data = file_get_contents($file['tmp_name']);
                $base64_image = base64_encode($file_data);
                echo "<p><span class='label'>Hình ảnh</span><img src='data:image/jpeg;base64,$base64_image' class='img'></p>";
            }
            echo "<p><button type='submit' class='submit'>Xác nhận</button></p>";
        }
        ?>
    </div>
</body>

</html>