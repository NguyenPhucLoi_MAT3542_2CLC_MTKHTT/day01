<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Student List</title>
    <style>
        .student-list {
            display: inline-block;
            margin: 50px 50px;
        }

        .label {
            display: inline-block;
            margin: 5px 34px 5px 100px;

        }

        .label_keyword {
            margin: 5px 15px 5px 100px;

        }

        .search {
            border: 2px solid blue;
            background-color: #06f;
            color: #fff;
            text-align: center;
            width: 120px;
            border-radius: 10px;
            padding: 15px 0px 10px;
            margin: 10px 0px 10px 220px;
        }

        #department {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 180px;
            padding: 10px 0px 5px;
            background-color: lightblue;
            margin: 0px 20px 20px;
        }

        #keyword {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 178px;
            padding: 10px 0px 5px;
            background-color: lightblue;
            margin: 0px 0px 10px 20px;
        }

        .button {
            width: 50px;
            border: 2px solid blue;
            background-color: #09f;
            color: #fff;
            padding: 5px;

        }

        th {
            padding: 0px 10px 5px 10px;
            text-align: left;
        }

        td {
            padding: 3px 10px 5px 10px;
            text-align: left;
        }

        th.action,
        td.action {
            padding-left: 100px;
        }

        .add-students {
            display: flex;
            align-items: center;
        }

        .add {
            border: 2px solid blue;
            background-color: #06f;
            color: #fff;
            text-align: center;
            width: 90px;
            border-radius: 10px;
            padding: 10px 0px 10px;
            margin: 10px 0px 0px 258px;
        }
    </style>
</head>

<body>
    <div class="student-list">
        <form method="get">
            <label for="department" class="label">Khoa</span></label>
            <select name="department" id="department">
                <option value=""></option>
                <?php
                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu',
                );

                foreach ($departments as $key => $value) {
                    $selected = ($_GET['department'] == $key) ? 'selected' : '';
                    echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
                }

                ?>
            </select><br>

            <label for="keyword" class="label_keyword">Từ khóa</label>
            <input type="text" name="keyword" id="keyword"><br>

            <button type="submit" class="search">Tìm kiếm</button>
        </form>
        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th class="action">Action</th>
            </tr>

            <?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "ltweb";

            $conn = new mysqli($servername, $username, $password, $dbname);

            if ($conn->connect_error) {
                die("Kết nối thất bại: " . $conn->connect_error);
            }
            $department = isset($_GET['department']) ? $_GET['department'] : '';
            $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';

            $sql = "SELECT * FROM students WHERE department LIKE '%$department%' AND (full_name LIKE '%$keyword%' OR email LIKE '%$keyword%')";
            $sql = "SELECT * FROM students";
            $result = mysqli_query($conn, $sql);
            $count = 1;

            $num_rows = mysqli_num_rows($result);
            echo "<div class='add-students'>";
            echo "<p>Số sinh viên tìm thấy: " . $num_rows . "</p>";
            echo '<form action="register.php">
        <button type="submit" class = "add">Thêm</button>
      </form>';
            echo "</div>";

            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $count . "</td>";
                echo "<td>" . $row['full_name'] . "</td>";
                echo "<td>" . $row['department'] . "</td>";
                echo '<td class="action"><button onclick="deleteStudent(' . $row['id'] . ')" class="button">Xóa</button> <button onclick="editStudent(' . $row['id'] . ')" class="button">Sửa</button></td>';
                echo "</tr>";
                $count++;
            }

            mysqli_close($conn);
            ?>
        </table>
    </div>

    <script>
        function deleteStudent(id) {
            confirm("Bạn có chắc chắn muốn xóa sinh viên này?") ? redirectTo("delete.php?id=" + id) : null;
        }

        function editStudent(id) {
            redirectTo("edit.php?id=" + id);
        }

        function redirectTo(url) {
            window.location.href = url;
        }
    </script>
</body>

</html>