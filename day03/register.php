<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #07f;
            display: inline-block;
            margin: 10% 20%;
        }

        .label_name {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: #09f;
            text-align: center;
            width: 90px;
            padding: 10px 0px 5px;
            margin: 70px 10px 5px 40px;
        }

        .label {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: #09f;
            text-align: center;
            width: 90px;
            padding: 10px 0px 5px;
            margin: 5px 10px 5px 40px;

        }

        .submit {
            border: 2px solid #07f;
            background-color: limegreen;
            color: #fff;
            border-radius: 10px;
            text-align: center;
            padding: 15px 0px 10px;
            margin: 20px 180px 40px;
            width: 130px;
        }

        .input_name {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 300px;
            padding: 10px 10px 5px;
            margin: 0px 50px 0px 0px;
        }

        #department {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 140px;
            padding: 10px 0px 5px;
        }

        .radio_gender {
            margin: 10px;
        }
    </style>
</head>

<body>
    <div class="register">
        <form>
            <label for="name" class="label_name">Họ và tên</label>
            <input type="text" class="input_name"><br>
            <label for="gender" class="label">Giới tính</label>
            <?php
            $genders = array(
                0 => 'Nam',
                1 => 'Nữ'
            );

            for ($i = 0; $i < count($genders); $i++) {
                $genderValue = array_keys($genders)[$i];
                $genderText = $genders[$genderValue];

                echo '<label>';
                echo '<input type="radio" name="gender" class="radio_gender" value="' . $genderValue . '">' . $genderText;
                echo '</label>';
            }
            ?><br>
            <label for="" class="label">Phân khoa</label>
            <select name="department" id="department">
                <option value="">--Chọn phân khoa--</option>
                <?php
                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu'
                );

                foreach ($departments as $key => $value) {
                    echo '<option value="' . $key . '">' . $value . '</option>';
                }
                ?>
            </select><br>
            <button type="submit" class="submit">Đăng ký</button>
        </form>
    </div>
</body>

</html>