CREATE DATABASE ltweb;

CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255) ,
    gender ENUM('Nam', 'Nữ') ,
    department VARCHAR(255) ,
    birthdate DATE ,
    address VARCHAR(255),
    image longblob DEFAULT 
);