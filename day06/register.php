<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #07f;
            display: inline-block;
            margin: 10% 20%;
        }

        .label {
            display: inline-block;
            border: 2px solid #07f;
            color: #fff;
            background-color: limegreen;
            text-align: center;
            width: 90px;
            padding: 10px 0px 5px;
            margin: 5px 10px 5px 40px;

        }

        .submit {
            border: 2px solid #07f;
            background-color: limegreen;
            color: #fff;
            text-align: center;
            width: 130px;
            border-radius: 10px;
            padding: 15px 0px 10px;
            margin: 10px 180px 20px;
        }

        .input_name {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 300px;
            padding: 10px 10px 5px;
            margin: 0px 50px 0px 0px;
        }

        #department {
            display: inline-block;
            border: 2px solid #07f;
            outline: none;
            width: 140px;
            padding: 10px 0px 5px;
        }

        .radio_gender {
            margin: 10px;
        }

        .input_birthdate {
            display: inline-block;
            border: 2px solid #07f;
            width: 136px;
            outline: none;
            padding: 10px 0px 5px;
        }

        .address {
            display: inline-block;
            border: 2px solid #07f;
            width: 320px;
            outline: none;
            padding: 10px 0px 10px;
        }

        .errorMessage {
            color: red;
            margin: 10px;
        }

        .list {
            list-style-type: none;
        }

        span {
            color: red;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#registrationForm").submit(function(event) {
                event.preventDefault();

                var errors = [];

                var name = $(".input_name").val();
                if (name.trim() === '') {
                    errors.push("Hãy nhập tên.");
                }

                var department = $("#department").val();
                if (department === '') {
                    errors.push("Hãy chọn phân khoa.");
                }

                var birthdate = $(".input_birthdate").val();
                if (birthdate.trim() === '') {
                    errors.push("Hãy nhập ngày sinh.");
                } else {
                    var birthdate_test = /^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/\d{4}$/;
                    if (!birthdate_test.test(birthdate)) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng (dd/mm/yyyy).");
                    }
                }

                var message = "";
                if (errors.length > 0) {
                    message = '<ul class= "list">';
                    for (var i = 0; i < errors.length; i++) {
                        message += '<li>' + errors[i] + '</li>';
                    }
                    message += '</ul>';
                }
                $(".errorMessage").html(message);

                if (errors.length === 0) {
                    $(this).unbind('submit').submit();
                }
            });
        });
    </script>
</head>

<body>
    <div class="register">
        <div class="errorMessage"></div>
        <form id="registrationForm" method="POST" action="confirm.php" enctype="multipart/form-data">
            <label for="name" class="label">Họ và tên<span class="star"> *</span></label>
            <input type="text" class="input_name" name="name"><br>
            <label for="gender" class="label">Giới tính<span class="star"> *</span></label>
            <?php
            $genders = array(
                0 => 'Nam',
                1 => 'Nữ'
            );

            $count = count($genders);
            $keys = array_keys($genders);

            for ($i = 0; $i < $count; $i++) {
                $genderValue = $keys[$i];
                $genderText = $genders[$genderValue];
                echo '<label>';
                echo '<input type="radio" name="gender" class="radio_gender" value="' . $genderValue . '">' . $genderText;
                echo '</label>';
            }
            ?><br>
            <label for="" class="label">Phân khoa<span class="star"> *</span></label>
            <select name="department" id="department">
                <option value="">--Chọn phân khoa--</option>
                <?php
                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu'
                );

                foreach ($departments as $key => $value) {
                    echo '<option value="' . $key . '">' . $value . '</option>';
                }
                ?>
            </select><br>
            <label for="birthdate" class="label">Ngày sinh<span class="star"> *</span></label>
            <input type="text" class="input_birthdate" placeholder="   dd/mm/yyyy" name="birthdate"><br>
            <label for="" class="label">Địa chỉ</label>
            <input type="text" class="address" name="address"><br>
            <label for="" class="label">Hình ảnh</label>
            <input type="file" name="imageUpload" id="imageUpload"><br>
            <button type="submit" class="submit">Đăng ký</button>
        </form>
    </div>
</body>

</html>