<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ltweb";

// Tạo kết nối
$conn = new mysqli($servername, $username, $password, $dbname);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối thất bại: " . $conn->connect_error);
}
if (isset($_POST["submit"])) {

    $fullname = $_POST['name'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];

    if ($gender === '0') {
        $gender = 'Nam';
    } elseif ($gender === '1') {
        $gender = 'Nữ';
    }

    if ($department === 'MAT') {
        $department = 'Khoa học máy tính';
    } elseif ($department === 'KDL') {
        $department = 'Khoa học vật liệu';
    }

    // Chuyển đổi định dạng ngày sinh
    $birthdate = DateTime::createFromFormat('d/m/Y', $birthdate)->format('Y-m-d');

    // Chuẩn bị truy vấn SQL và chèn dữ liệu vào bảng students
    $sql = "INSERT INTO students (full_name, gender, department, birthdate, address, image) 
    VALUES (?, ?, ?, ?, ?, ?)";

    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssssss", $fullname, $gender, $department, $birthdate, $address, $image);

    if (isset($_FILES['imageUpload'])) {
        $file = $_FILES['imageUpload'];
        $file_data = file_get_contents($file['tmp_name']);
        $image = base64_encode($file_data);
    } else {
        $image = '';
    }

    if ($stmt->execute()) {
        echo "Dữ liệu đã được lưu vào cơ sở dữ liệu.";
    } else {
        echo "Lỗi khi lưu dữ liệu: " . $stmt->error;
    }

    $stmt->close();
}

$conn->close();
?>